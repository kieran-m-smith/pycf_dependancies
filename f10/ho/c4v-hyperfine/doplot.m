% plot us and Popova and Wells

clf reset

% 5I7_1 Wells 
yoffset=1e-2
xoffset=0;
%xoffset=0.178;
scale  =1e-1

load JPR_5I7_doublet.dat;
specdat = JPR_5I7_doublet;

specdat(:,2) = ...
 (max(specdat(:,2)) - specdat(:,2))*scale+yoffset; 
load simulated5I71.dat
%plot(simulated5I71(:,1),simulated5I71(:,3))
%pause
%plot(specdat(:,1),specdat(:,2))
%pause
plot(simulated5I71(:,1)+xoffset,simulated5I71(:,3),'-',...
   specdat(:,1), specdat(:,2),'-')
v=axis;
v(1)=5254.5;
v(2)=5258.5;
axis(v);
title('^5I_7(1)');
grid on
pause



% 5I7_2 Popova 
yoffset=2e-3
xoffset=0;
%xoffset=-0.01;
scale  =3e-6
load popovaspectrum.dat
popovaspectrum(:,2) = ...
 (max(popovaspectrum(:,2)) - popovaspectrum(:,2))*scale+yoffset; 
load simulated5I72.dat
%plot(simulated(:,1),simulated(:,3))
%pause
%plot(popovaspectrum(:,1),popovaspectrum(:,2))
%pause
plot(simulated5I72(:,1)+xoffset,simulated5I72(:,3),'-',...
   popovaspectrum(:,1), popovaspectrum(:,2),'-')
v=axis;
v(1)=5271;
v(2)=5275;
axis(v);
title('5I_7(2)');
grid on
pause


% 5I6_1 Wells 
yoffset=5e-4
xoffset=0;
%xoffset=-0.34; 
scale  =5e-15

load JPR_5I6_doublet.dat;
specdat = JPR_5I6_doublet;

specdat(:,2) = ...
 (max(specdat(:,2)) - specdat(:,2))*scale+yoffset; 
load simulated5I61.dat
%plot(simulated5I61(:,1),simulated5I61(:,3))
%pause
%plot(specdat(:,1),specdat(:,2))
%pause
plot(simulated5I61(:,1)+xoffset,simulated5I61(:,3),'-',...
   specdat(:,1), specdat(:,2),'-')
v=axis;
v(1)=8752;
v(2)=8756;
axis(v);
title('^5I_6(1)');
grid on
pause


