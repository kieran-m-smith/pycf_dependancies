set title "5I6 first group                                             "
set xrange [0.000000:0.040000]
set autoscale y
set ylabel "energy (cm-1)"
set xlabel "value of al        "
set terminal postscript
set output "split_5I6.ps"
plot "split_5I6" using 1:2 notitle with lines,"split_5I6" using 1:3 notitle with lines,"split_5I6" using 1:4 notitle with lines,"split_5I6" using 1:5 notitle with lines,"split_5I6" using 1:6 notitle with lines,"split_5I6" using 1:7 notitle with lines,"split_5I6" using 1:8 notitle with lines,"split_5I6" using 1:9 notitle with lines,"split_5I6" using 1:10 notitle with lines,"split_5I6" using 1:11 notitle with lines,"split_5I6" using 1:12 notitle with lines,"split_5I6" using 1:13 notitle with lines,"split_5I6" using 1:14 notitle with lines,"split_5I6" using 1:15 notitle with lines,"split_5I6" using 1:16 notitle with lines,"split_5I6" using 1:17 notitle with lines
set output
set terminal x11
replot
pause -1 "Press RETURN to continue"
