% plot us and Popova and Wells

clf reset

% 5I7_1 Wells and Popova 
yoffset=1e-2
xoffset=0;
%xoffset=0.178;
scale  =1e-1

load JPR_5I7_doublet.dat;
specdat = JPR_5I7_doublet;

%specdat(:,2) = ...
% (max(specdat(:,2)) - specdat(:,2))*scale+yoffset; 
specdat(:,2) = -log10(specdat(:,2));
specdat(:,2) = ...
  (specdat(:,2) - min(specdat(:,2)))*scale+yoffset; 

pyoffset=3e-2
pxoffset=0;
%pxoffset=0.178;
pscale  =0.5e-1
load popovay1.dat;
popovaspectrum=popovay1;
popovaspectrum(:,2) = -log10(popovaspectrum(:,2));
popovaspectrum(:,2) = ...
  (popovaspectrum(:,2) - min(popovaspectrum(:,2)))*pscale+pyoffset; 

load simulated5I71_4k.dat
%plot(simulated5I71(:,1),simulated5I71(:,3))
%pause
%plot(specdat(:,1),specdat(:,2))
%pause
plot(simulated5I71_4k(:,1)+xoffset,simulated5I71_4k(:,3),'-',...
   specdat(:,1), specdat(:,2),'-',...
   popovaspectrum(:,1), popovaspectrum(:,2),'-')

v=axis;
v(1)=5254.5;
v(2)=5258.5;
axis(v);
title(['^5I_7(1) [-log10(I) plot] scale ' num2str(scale)]);
grid on
pause



% 5I7_2 Popova 
yoffset=2e-3
xoffset=0;
%xoffset=-0.01;
%scale  =3e-6
scale =2e-2
load popovaspectrum.dat
%popovaspectrum(:,2) = ...
% (max(popovaspectrum(:,2)) - popovaspectrum(:,2))*scale+yoffset; 
popovaspectrum(:,2) = -log10(popovaspectrum(:,2));
popovaspectrum(:,2) = ...
  (popovaspectrum(:,2) - min(popovaspectrum(:,2)))*scale+yoffset; 


load simulated5I72_4k.dat
%plot(simulated(:,1),simulated(:,3))
%pause
%plot(popovaspectrum(:,1),popovaspectrum(:,2))
%pause
plot(simulated5I72_4k(:,1)+xoffset,simulated5I72_4k(:,3),'-',...
   popovaspectrum(:,1), popovaspectrum(:,2),'-')
v=axis;
v(1)=5271;
v(2)=5275;
axis(v);
title(['5I_7(2) [-log10(I) plot] scale ' num2str(scale)]);
grid on
pause


% 5I6_1 Wells 
yoffset=5e-4
xoffset=0;
%xoffset=-0.34; 
%scale  =5e-15
scale  =6e-3

load JPR_5I6_doublet.dat;
specdat = JPR_5I6_doublet;

%specdat(:,2) = ...
% (max(specdat(:,2)) - specdat(:,2))*scale+yoffset; 
specdat(:,2) = -log10(specdat(:,2));
specdat(:,2) = ...
  (specdat(:,2) - min(specdat(:,2)))*scale+yoffset; 


load simulated5I61_4k.dat
%plot(simulated5I61(:,1),simulated5I61(:,3))
%pause
%plot(specdat(:,1),specdat(:,2))
%pause
plot(simulated5I61_4k(:,1)+xoffset,simulated5I61_4k(:,3),'-',...
   specdat(:,1), specdat(:,2),'-')
v=axis;
v(1)=8752;
v(2)=8756;
axis(v);
title(['^5I_6(1) [-log10(I) plot] scale ' num2str(scale)] );
grid on
pause


% 5I7_6 Wells and Popova 
yoffset=2e-3
xoffset=0;
%xoffset=0.178;
scale  =5e-3

load JPR_5I7_singlet.dat;
specdat = JPR_5I7_singlet;

%specdat(:,2) = ...
% (max(specdat(:,2)) - specdat(:,2))*scale+yoffset; 
specdat(:,2) = -log10(specdat(:,2));
specdat(:,2) = ...
  (specdat(:,2) - min(specdat(:,2)))*scale+yoffset; 

pyoffset=3e-3
pxoffset=0;
%pxoffset=0.178;
pscale  =1e-2
load popovay6.dat;
popovaspectrum=popovay6;
popovaspectrum(:,2) = -log10(popovaspectrum(:,2));
popovaspectrum(:,2) = ...
  (popovaspectrum(:,2) - min(popovaspectrum(:,2)))*pscale+pyoffset; 

calcoffset=-2.0

load simulated5I76_4k.dat
plot(simulated5I76_4k(:,1)+calcoffset,simulated5I76_4k(:,3),'-',...
   specdat(:,1), specdat(:,2),'-',...
   popovaspectrum(:,1), popovaspectrum(:,2),'-')

%v=axis;
%v(1)=5254.5;
%v(2)=5258.5;
%axis(v);
title(['^5I_7(6) [-log10(I) plot] scale ' num2str(scale) ' calcoffset' ...
		   ' ' num2str(calcoffset)]);
grid on
pause

