
% plot JPR

load JPR_5I6_doublet.dat;
specdat = JPR_5I6_doublet;
plot(specdat(:,1),-log10(specdat(:,2)) -min(-log10(specdat(:,2))) );
title('JPR 5I6 doublet.dat Log Spectra');
grid on
pause




load JPR_5I7_doublet.dat;
specdat = JPR_5I7_doublet;
plot(specdat(:,1),-log10(specdat(:,2)) -min(-log10(specdat(:,2))) );
title('JPR 5I7 doublet.dat Log Spectra');
grid on
pause


load JPR_5I7_singlet.dat;
specdat = JPR_5I7_singlet;
plot(specdat(:,1),-log10(specdat(:,2)) -min(-log10(specdat(:,2))) );
title('JPR 5I7 singlet.dat Log Spectra');
grid on
pause
