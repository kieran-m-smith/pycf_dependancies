% plot us and popova
yoffset=2e-3
xoffset=0.065;
scale  =3e-6
load popovaspectrum.dat
popovaspectrum(:,2) = ...
 (max(popovaspectrum(:,2)) - popovaspectrum(:,2))*scale+yoffset; 
load simulated.dat
%plot(simulated(:,1),simulated(:,3))
%pause
%plot(popovaspectrum(:,1),popovaspectrum(:,2))
%pause
plot(simulated(:,1)+xoffset,simulated(:,3),'-',...
   popovaspectrum(:,1), popovaspectrum(:,2),'-')
v=axis;
v(1)=5271;
v(2)=5275;
axis(v);
title('Experimental and Calulated Spectra');

