
% plot JPR

load JPR_5I6_doublet.dat;
specdat = JPR_5I6_doublet;
plot(specdat(:,1),specdat(:,2));
title('JPR_5I6_doublet.dat Spectra');
grid on
pause




load JPR_5I7_doublet.dat;
specdat = JPR_5I7_doublet;
plot(specdat(:,1),specdat(:,2));
title('JPR_5I7_doublet.dat Spectra');
grid on
pause


load JPR_5I7_singlet.dat;
specdat = JPR_5I7_singlet;
plot(specdat(:,1),specdat(:,2));
title('JPR_5I7_singlet.dat Spectra');
grid on
pause
