#!/usr/bin/env python

from __future__ import division
import numpy as np

import pycf.cfl as cfl
from pycf.import_sljm import ImportSLJM
from pycf.cfl_util import *
from pycf.spinh import SpinH
from numpy import linalg as LA

I = np.complex(0,1)
# Import the matrix elements.
t = ImportSLJM("../../f2cf")
thfs = ImportSLJM("../../f2cf_hfs")


#t.print_names()

MTOT = t.M0 + 0.56*t.M2 + 0.31*t.M4
PTOT = t.P2 + 0.5*t.P4 + 0.1*t.P6

hfsMTOT = thfs.M0 + 0.56*thfs.M2 + 0.31*thfs.M4
hfsMTOT.name = 'MTOT'
hfsPTOT =  thfs.P2 + 0.5*thfs.P4 + 0.1*thfs.P6
hfsPTOT.name = 'PTOT'

# Z, X, and Y magnetic fields times Bohr magnetion in cm-1/T.
mu_b = 0.466860
MZ = mu_b * t.MAGZ
MX = mu_b * t.MAGX
MY = mu_b * t.MAGY
hfsMZ = mu_b * thfs.MAGZ
hfsMZ.name = 'MZ'
hfsMX = mu_b * thfs.MAGX
hfsMX.name = 'MX'
hfsMY = mu_b * thfs.MAGY
hfsMY.name = 'MY'

t_list = [t.EAVG, t.F2, t.F4, t.F6, t.ALPHA, t.BETA, t.GAMMA,
        t.ZETA, t.C20, t.C22, t.C40, t.C42, t.C44, t.C60, t.C62, t.C64, t.C66, MTOT, PTOT, MX, MY, MZ]

thfs_list = [thfs.EAVG, thfs.F2, thfs.F4, thfs.F6, thfs.ALPHA, thfs.BETA,
        thfs.GAMMA,
        thfs.ZETA, thfs.C20, thfs.C22, thfs.C40, thfs.C42,
        thfs.C44, thfs.C60, thfs.C62, thfs.C64, thfs.C66, hfsMTOT, hfsPTOT, thfs.HYP, thfs.EQHYP, hfsMX, hfsMY, hfsMZ]

coeff = {
'EAVG' :                 10026.74, 
'F2'   :                 67987.0, 
'F4'   :                 50258.0,
'F6'   :                 34966.0,
'ALPHA':                 14.00, 
'BETA' :                -352.6, 
'GAMMA':               -26.0,
'ZETA' :                684.7,
'MTOT' :                -8.2,
'PTOT' :                1228.40,
#
'MZ'   :                       0.00,
'MX'   :                       0.00,
'MY'   :                       0.00,
'C20'        :              -704.0,
'C22'        :               -86.0,
'C40'        :              2205.0,
'C42'        :               781.0,
'C44'        :                453.0,
'C60'        :                708.0,
'C62'        :              -354.0,
'C64'        :              -1036.0,
'C66'        :              -1138.0,
'HYP'        :                0.015,
'EQHYP'      :               0.0062,

}


def gen_b_spiral(B0, n):
    t = np.linspace(-1,1,n)
    B = np.array([[B0*np.sqrt(1-t**2)*np.cos(6*np.pi*t)], [B0*np.sqrt(1-t**2)*np.sin(6*np.pi*t)], [B0*t]]).reshape(3,n)

    return B.transpose()



# Number of cores: Ivy Bridge - 24, Sandy bridge - 16
# Run SB for now, so:
#   + 1 for full 4f
#   + 7 for RH data
#   + 12 for ground hfs
#   + 12 for excited b spiral


# Set weighting factors
e_w = 3e-3               # Electronic levels
rh_w = 1e8              # 80 MHz RH data 
g_hfs_w = 1e5          # Ground state g, hfs
hfs_w = 6e6            # Ground state zero-field hfs
ge_w = 50e5             # Excited state g

# B spiral for ground and excited state
n = 12      # Number of field data points to be used
B0 = 0.05   # Max field strength in Tesla
B_list = gen_b_spiral(B0,n)

## Stephen's parameters; includes superconducting cavity zero-field data
#gg = np.array([[2.8976, -2.9451, -3.5568], [-2.9451, 8.9003, 5.5683], [-3.5568, 5.5683, 5.1208]])
#Ag = np.array([[274.2878, -202.5226, -350.8188], [-202.5226, 827.5043, 635.1453], [-350.8188, 635.1453, 706.1526]])
#Qg = np.array([[10.3950, -9.1166, -9.9576], [-9.1166, -5.9530, -14.3238], [-9.9576, -14.3238, -4.4420]])

# Stephen's new parmas that fit to RH data
#Ag = np.array([[ 291.8390,-232.0352,-302.3498], 
#[-232.0352, 706.0735, 727.7255],
#[-302.3498, 727.7255, 700.3551]])

#Qg = np.array([[   8.8453,  -9.5384, -13.7641],
#[  -9.5384,  -5.9900, -15.1250],
#[ -13.7641, -15.1250,  -2.8553]])

#gg = np.array([[   2.9132,  -2.9849,  -3.5967],
#[  -2.9849,   8.8626,   5.5350],
#[  -3.5967,   5.5350,   5.7167]])

# Sun et al. excited state g
#ge = np.array([[1.950, -2.212, -3.584], [-2.212, 4.232, 4.986], [-3.584, 4.986, 7.888]])

# Zero field Hamiltonian and energies.
h = cfl.Hamiltonian(t_list)
h.set_coeff(coeff)
h_list = [h]
ex = np.loadtxt('pryso_site1_energy.txt', skiprows=1)
exdata_list = [cfl.ExData(ex)]
weights_list = [e_w]

# Zero-Field Hyperfine data - difference from first level
exzfs = np.array([ 
# ground state
                   [1, 3, 17.3],
                   [1, 5, 27.49],
# excited state 
        #           [384, 386, 4.84],
        #           [384, 388, 9.43]
                 ])
print(exzfs)
# MHz to cm-1
exzfs[:,2] *= 1/29.9702547e3
print(exzfs) 
exdata_list += [cfl.ExData(exzfs, 'D')]
h = cfl.Hamiltonian(thfs_list)
#coeff['MX'] = B[0]
#coeff['MY'] = B[1]
#coeff['MZ'] = B[2]
h.set_coeff(coeff)
h_list += [h]
weights_list += [g_hfs_w]

# Raman-heterodyne data. 

# Low freq (80 MHz) RH data, ac line - need  unique field values because of
# sample orientation. 

# Generate field values in mT (0, then 0.5 in each x, y, and z magnet
# coordinates)
#lf_bvals = [np.zeros(3)]
#v = 0.5
#for i in range(3):
#    B = np.zeros(3)
#    B[i] = v 
#    lf_bvals += [np.copy(B)]

# Curvature tensor and offset (MHz and MHz/mT^2)
#f0 = 85.096939
#M = np.array([[15.7, 30.1, -40. ], [ 30.1,  67.6, -82.5], [-40., -82.5, 104.3]])

#for B in lf_bvals:
#    fcalc = f0 + np.transpose(B).dot(M).dot(B)
#    fcalc = fcalc/29.9702547e3  # Convert from MHz to cm-1
#    exdata_list += [cfl.ExData(np.array([[137, 138, fcalc]]), 'D')]
#    
#    B = B*1e-3   # Convert to T
#    h = cfl.Hamiltonian(thfs_list)
#    coeff['MX'] = B[0]
#    coeff['MY'] = B[1]
#    coeff['MZ'] = B[2]
#    h.set_coeff(coeff)
#    h_list += [h]
#    weights_list += [rh_w]

# High freq. (800 MHz) RH data (in polynomial form). Sweeps of xcoil,
# corresponds to MY in cryst coords (MHz & mT). 

#rh_ls = ['n', 'i', 't', 'l', 'p', 'ab', 'a', 'm', 'h', 'e', 'd', 'f', 'g']

#hf_pd = {'a': np.array([ -66.15555132,    6.39162656,  930.61283071]),
#        'ab': np.array([ -69.04862725,    5.41291306,  952.53418828]),
#        'e': np.array([ -7.37903518e-01,  -7.54025848e-02,   8.79316067e+02]),
#        'd': np.array([  7.24218971e-01,   4.02538296e-02,   8.79375239e+02]),
#        'g': np.array([ -2.40721395e+00,  -1.07639045e-01,   8.80549189e+02]),
#        'f': np.array([  1.79185958e+00,   1.30468394e-01,   8.79264214e+02]),
#        'i': np.array([  -16.77488309,    -1.95603484,  1010.3429402 ]),
#        'h': np.array([ -1.19913355e+00,  -1.91767706e-01,   8.23918680e+02]),
#        'k': np.array([ -45.15444527,   -2.78151756,  818.58787975]),
#        'j': np.array([  2.18620204e+01,   4.77922935e-01,   8.25555479e+02]),
#        'm': np.array([  1.78788342e+00,   1.45801605e-01,   7.74219752e+02]),
#        'l': np.array([  19.61210058,    2.27686832,  752.61764749]),
#        'n': np.array([   34.30221452,     5.33369394,  1096.58093532]),
#        'p': np.array([   28.76980869,     2.75856858,  1167.87303762]),
#        'b': np.array([  1.13813883e-01,   8.96736961e+02]),
#        'c': np.array([  1.13813883e-01,   8.96736961e+02]),
#        't': np.array([ -33.78216636,   -3.17673515,  667.08738712]),
#        'w': np.array([  -2.45056251,   -0.75119208,  726.13596956]),
#        'v': np.array([-303.52686482,   14.53612923,  612.24778816]),
#        'x': np.array([  54.99637342,    3.57944151,  717.38335428]),
#        'z': np.array([ -55.69957211,    4.80461517,  802.89630244])}

# Lets try some radical new assignments, just for the shits of it. Note: the
# good fit from early days had "n i p a ab". No v!
#hf_li = {
#        # First set of 4 are solid; linked to 85 MHz ac line via 136
#        'n': [135, 138],
#        'i': [135, 137],
#        't': [136, 137],
#        'l': [136, 138],
#        'f': [131, 133], # only valid at 0.5 mT
#        'g': [132, 134], # only valid at 0.5 mT
#        # Next 3 seem solid (in particular, 'ab' & 'a'
#        'p': [7, 10],
#        'ab':[6, 8],
#        'a': [9, 11],
#        # Maybe not actually correct? Could be line below that one with identical shape
#        #'v': [6, 7],
#        # It seems like 'm' and 'h' are solid
#        'm': [7, 9],
#        'h': [8, 10],
#        # Almost have to be correct, no other gs lines around... 
#        'e': [1, 3], # only valid at 0.5 mT
#        'd': [2, 4], # only valid at 0.5 mT
#        # Unclear for now... 137-139 might be at kite level
#        #'z': [137, 139], # only valid at 0 mT
#        #'x': [138, 140], # only valid at 0 mT
#        # Re-assigned to 131-133 and 132-134
#        #'b': [130, 132], # only valid at 0 mT
#        #'c': [129, 131], # only valid at 0 mT
#        }

#hf_bvals = np.zeros([3,3])
#hf_bvals[1,1] = 0.5
#hf_bvals[2,1] = 0.3

#for ii,B in enumerate(hf_bvals):
#    rh_ex = []
#    for i,k in enumerate(rh_ls):
#        if (k in ['z', 'x', 'b', 'c', 'v']) & (ii != 0):
#            continue
#        elif (k in ['f', 'g', 'e', 'd']) & (ii != 1):
#            continue
#        else:
#            p = np.poly1d(hf_pd[k])
#            fcalc = p(B[1])/29.9702547e3  # Convert from MHz to cm-1
#            rh_ex += [hf_li[k]+[fcalc]]
#
#    exdata_list += [cfl.ExData(np.array(rh_ex), 'D')]
#    
#    B = B*1e-3   # Convert to T
#    h = cfl.Hamiltonian(thfs_list)
#    coeff['MX'] = B[0]
#    coeff['MY'] = B[1]
#    coeff['MZ'] = B[2]
#    h.set_coeff(coeff)
#    h_list += [h]
#    weights_list += [rh_w]


# Ground state hfs & g from SH
#for B in B_list:
#    sh = SpinH(['bgs', 'ias', 'iqi', 'bi'], B = B, S = 1/2, I = 7/2)
#    sh.add_term('bgs', gg)
#    sh.add_term('ias', Ag)
#    sh.add_term('iqi', Qg)
#    sh.add_term('bi', 0.1618)
#
#    w, v = LA.eig(sh.get_H())
#    E = w.real
#    E = np.sort(E - min(E))

#    E = E/29.9702547e3
#    # Ground state energy differences, w.r.t. first level.
#    exdata_list += [cfl.ExData(np.array([[1,i+2, e] for i,e in enumerate(E[1:])]), 'D')]
#    h = cfl.Hamiltonian(thfs_list)
#    coeff['MX'] = B[0]
#    coeff['MY'] = B[1]
#    coeff['MZ'] = B[2]
#    h.set_coeff(coeff)
#    h_list += [h]
#    weights_list += [g_hfs_w]

## Excited state g - Sun. 
#for B in B_list:
#    sh = SpinH(['bgs'], B = B, S = 1/2)
#    sh.add_term('bgs', ge)
#
#    w, v = LA.eig(sh.get_H())
#    E = w.real
#    E = np.sort(E - min(E))
#    E = E/29.9702547e3

#    # Excited state energy differences. First 4I13/2 level is 17 (1 based
#    # indexing, no hyperfine).
#    exdata_list += [cfl.ExData(np.array([[17, 18, E[1]]]), 'D')]
#
#    h = cfl.Hamiltonian(t_list)
#    coeff['MX'] = B[0]
#    coeff['MY'] = B[1]
#    coeff['MZ'] = B[2]
#    h.set_coeff(coeff)
#    h_list += [h]
#    weights_list += [ge_w]


# Optimization bounds and stepsize for the basinhopping algorithm.
bounds = bal_bounds(coeff, {
    'EAVG' :  100,
    'F2'   :  200,
    'F4'   :  200,
    'F6'   :  200,
    'ZETA' :  5,
    'C20'  :  250,
    'C22'  :  250,
    'C40'  :  300,
    'C42'  :  250,
    'C44'  :  300,
    'C60'  :  250,
    'C62'  :  200,
    'C64'  :  200,
    'C66'  :  200,
    'HYP'  :  0.01,
    'EQHYP':  0.01,
    'ALPHA':  20,   
    'BETA' :  20,
    'GAMMA':  200,
    'T2'   :  20,
    'T3'   :  20,
    'T4'   :  20,
    'T6'   :  20,
    'T7'   :  20,
    'T8'   :  20,
    'MTOT' :  10,
    'PTOT' : 300,
    })


stepsize = {
    'EAVG' :  50,
    'F2'   :  50,
    'F4'   :  50,
    'F6'   :  50,
    'ZETA' :  0.1,
    'C20'  :  20,
    'C21'  :  20,
    'C22'  :  20,
    'C40'  :  20,
    'C41'  :  20,
    'C42'  :  20,
    'C43'  :  20,
    'C44'  :  20,
    'C60'  :  20,
    'C61'  :  20,
    'C62'  :  20,
    'C63'  :  20,
    'C64'  :  20,
    'C65'  :  20,
    'C66'  :  20,
    'HYP'  :  0.0005,
    'EQHYP':  0.0005,
    'ALPHA':  0.1,
    'BETA' :  0.1,
    'GAMMA':  0.1,
    'MTOT' :  0.1,
    'PTOT' :  20,
    }


#param = ['EAVG', 'HYP', 'EQHYP']

param = ['EAVG', 'ALPHA', 'BETA', 'GAMMA', 'ZETA', 'MTOT', 'PTOT', 'F2', 'F4', 'F6', 'C20', 'C22', 'C40', 'C42', 'C44', 'C60', 'C62', 'C64', 'C66', 'HYP', 'EQHYP'] 
#cfl_min = cfl.CFLMin('nlopt_bobyqa', xtol=1e-5, bounds=bounds, cov=False, dry_run=False, maxtime=518400)

cfl_min = cfl.CFLMin('basinhopping', niter=1, lmin='nlopt_bobyqa', xtol=1e-2,
   bounds=bounds, stepsize=stepsize, step_adapt_int=25)


res = cfl.mh_fit(param, h_list, weights_list, exdata_list, cfl_min)

#print("Number of Hamiltonians: {}".format(len(h_list)))
#print(res['summary'])

with open("2020-04-21_pryso_site1_test.txt", "w") as summary_file:
    summary_file.write(res['summary'])


