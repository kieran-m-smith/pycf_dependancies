#!/usr/bin/env python

from __future__ import division
import numpy as np

import pycf.cfl as cfl
from pycf.import_sljm import ImportSLJM
from pycf.cfl_util import *
from pycf.spinh import SpinH
from numpy import linalg as LA
import pycf.pyemp as pyemp

I = np.complex(0,1)
# Import the matrix elements.
t = ImportSLJM("../../f2cf")
thfs = ImportSLJM("../../f2cf_hfs")

Spectrum = pyemp.Spectrum(name = 'pr_yso', emproot = '/home/users/mfr24/linuxemp/', states = t, addtensor)
